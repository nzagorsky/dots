"""
Module to keep layers.
Each layer must contain `install` and `setup` method.
`install` method manages proper installation, `setup` method sets up dotfiles, systemd and configs.
"""


import os
import shutil
import subprocess
import sys
import inspect

from queue import Queue

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
LAYERS_LIST = os.listdir(os.path.join(SCRIPT_DIR, "layers"))
USER_PATH = os.path.expanduser("~")


class Installation(object):
    def install_all(self):
        layers = Layers()

        queue = Queue(maxsize=len(layers.registered_layers.items()))

        installed = []

        for layer_name, layer in layers.registered_layers.items():
            queue.put((layer_name, layer))

        while not queue.empty():
            layer_name, layer = queue.get()

            dependency_waiting = False
            for dependency in layer.depends_on:
                if dependency not in installed:
                    print(
                        f"... DEBUG: No dependency ({dependency}) for {layer.name}. Placing back in queue"
                    )
                    queue.put((layer_name, layer))
                    dependency_waiting = True
                    break

            if dependency_waiting:
                continue

            print(f">>> Installing {layer.name}")
            layer().install()
            layer().setup()
            check_result = layer().check()
            if not check_result:
                print(f"Layer: {layer.name} failed")
                sys.exit(1)

            installed.append(layer_name)


def layer_folder(layer):
    return os.path.join(SCRIPT_DIR, "layers", layer.name)


def call_system(command, exit_on_error=True):
    # response = subprocess.check_output(command, shell=True)
    popen = subprocess.Popen(
        command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    out, err = popen.communicate()
    if popen.returncode > 0:

        if exit_on_error is False:
            return False

        if err and "systemd" in err.decode("utf-8"):
            return

        print(command)
        out and print(out.decode("utf-8"))
        err and print(err.decode("utf-8"))
        raise SystemExit(popen.returncode)
    return True


def install_pacman(package_name):
    call_system(f"sudo pacman -S {package_name} --needed --noconfirm --quiet")


def install_aur(package_name):
    call_system(f"pikaur -S --noconfirm {package_name} --quiet")


def install_flatpak(package_name):
    call_system(f"flatpak install --user -y {package_name}")


def setup_symlink(layer):
    for source, link_name in layer.links.items():
        source_path = os.path.join(layer_folder(layer), source)
        is_directory = os.path.isdir(source_path)
        link_name = link_name.replace("~", os.environ["HOME"])

        if os.path.exists(link_name):
            if os.path.isdir(link_name) and not os.path.islink(link_name):
                shutil.rmtree(link_name)
            if os.path.exists(link_name) and not os.path.islink(link_name):
                os.remove(link_name)
            if os.path.islink(link_name):
                os.unlink(link_name)

        os.symlink(src=source_path, dst=link_name, target_is_directory=is_directory)
    return True


class LayersMixin(object):
    """
    Contains logic for managing layers to keep Layers class clean.
    """

    def __init__(self):
        self._verify_registration()

    def _verify_registration(self):
        """
        Check for layers with folders which are not registered.
        """
        for layer in LAYERS_LIST:
            if layer not in self.registered_layers:
                print(f"{layer} is not registered layer name.")
                sys.exit(1)

    @property
    def registered_layers(self):
        if not getattr(self, "_registered", None):
            self._registered = {}
            for element in dir(self):
                element_attr = getattr(self, element, None)
                if inspect.isclass(element_attr) and not element.startswith("_"):
                    if getattr(element_attr, "name", None):
                        self._registered[element_attr.name] = element_attr

        return self._registered


class Layers(LayersMixin):
    class Arch:
        """
        Keeps state of installation.
        """

        name = "arch"
        depends_on = []
        links = {}

        def install(self):
            return True

        def setup(self):
            return True

        def check(self):
            return True

    class ArchBase:
        name = "arch_base"
        depends_on = []
        links = {}

        def install(self):
            call_system("sudo pacman -Sy")
            install_pacman("base-devel")

        def setup(self):
            return True

        def check(self):
            return True

    class Ctags:
        name = "ctags"
        depends_on = []
        links = {".ctags.d": "~/.ctags.d"}

        def install(self):
            install_pacman("ctags")

        def setup(self):
            return setup_symlink(self)

        def check(self):
            return True

    class Docker:
        name = "docker"
        depends_on = []
        links = {}

        def install(self):
            install_pacman("docker")

        def setup(self):
            setup_command = """
            sudo systemctl start docker
            sudo systemctl enable docker

            sudo groupadd docker -f
            sudo usermod -a -G docker `whoami`
            """
            call_system(setup_command)

        def check(self):
            return True

    class Emacs:
        name = "emacs"
        depends_on = []
        links = {".emacs.d": "~/.emacs.d"}

        def install(self):
            install_pacman("emacs")

        def setup(self):
            return setup_symlink(self)

        def check(self):
            return True

    class Flatpak:
        name = "flatpak"
        depends_on = []
        links = {}

        def install(self):
            install_pacman("flatpak")

        def setup(self):
            call_system(
                "sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo"
            )

            call_system(
                "flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo"
            )
            return True

        def check(self):
            return True

    class Git:
        name = "git"
        depends_on = []
        links = {
            ".gitconfig": "~/.gitconfig",
        }

        def install(self):
            install_pacman("git")

        def setup(self):
            setup_symlink(self)

        def check(self):
            return True

    class Gnome:
        name = "gnome"
        depends_on = []
        links = {}

        # NOTE Currently disabled
        def install(self):
            return True

        def setup(self):
            return True

        def check(self):
            return True

    class Neovim:
        name = "neovim"
        depends_on = ["python", "node", "git"]
        links = {".config/nvim": "~/.config/nvim"}

        def install(self):
            install_pacman("neovim")
            install_pacman("xclip")

        def setup(self):
            setup_symlink(self)

            submodule_path = (
                f"{USER_PATH}/.config/nvim/repos/github.com/Shougo/dein.vim"
            )
            if not os.path.exists(submodule_path):
                call_system(
                    f"git clone https://github.com/Shougo/dein.vim {submodule_path}"
                )
            call_system(
                f"mkdir -p {USER_PATH}/.config/nvim/sessions {USER_PATH}/.config/nvim/undo"
            )
            return True

        def check(self):
            return True

    class Python:
        name = "python"
        depends_on = ["arch_base"]
        links = {}

        def install(self):
            install_pacman("python")
            install_pacman("python-pip")

        def setup(self):
            setup_command = """
            pip install --user -U --quiet  \
                bandit  \
                black  \
                docker-compose  \
                flake8  \
                ipdb  \
                ipython  \
                jedi==0.15.2  \
                mypy  \
                neovim  \
                pydocstyle  \
                pyflakes  \
                python-language-server[all]  \
                radon  \
                vim-vint  \
                yamllint
            """
            call_system(setup_command)

        def check(self):
            return True

    class Tmux:
        name = "tmux"
        depends_on = ["git"]
        links = {
            ".tmux": "~/.tmux",
            ".tmux.conf": "~/.tmux.conf",
        }

        def install(self):
            install_pacman("tmux")

        def setup(self):
            setup_symlink(self)
            submodule_path = f"{USER_PATH}/.tmux/plugins/tpm"

            if not os.path.exists(submodule_path):
                call_system(
                    f"git clone https://github.com/tmux-plugins/tpm {submodule_path}"
                )

        def check(self):
            return True

    class VirtManager:
        name = "virt-manager"
        depends_on = []
        links = {}

        def install(self):
            install_pacman("virt-manager")
            install_pacman("ebtables")
            install_pacman("iptables")
            install_pacman("dnsmasq")

        def setup(self):
            setup_command = """
            sudo groupadd libvirt -f
            sudo usermod -a -G libvirt `whoami`
            echo "Added to libvirt group. To use without password you must relog."

            sudo systemctl enable libvirtd
            sudo systemctl restart libvirtd
            """
            call_system(setup_command)

        def check(self):
            return True

    class Xfce4Terminal:
        name = "xfce4-terminal"
        depends_on = []
        links = {".config/xfce4/terminal": "~/.config/xfce4/terminal"}

        def install(self):
            install_pacman("xfce4-terminal")

        def setup(self):
            call_system(f"mkdir -p {USER_PATH}/.config/xfce4")
            setup_symlink(self)
            return True

        def check(self):
            return True

    class Zerotier:
        name = "zerotier"
        depends_on = []
        links = {}

        def install(self):
            install_pacman("zerotier-one")

        def setup(self):
            setup_command = """
            # Ignore bridges and docker by zerotier
            sudo bash -c "cat > /var/lib/zerotier-one/local.conf"  <<EOF
            {
                "settings": {
                    "interfacePrefixBlacklist": [ "docker", "br-" ]
                }
            }
            EOF


            sudo systemctl enable zerotier-one.service
            sudo systemctl start zerotier-one.service

            sudo zerotier-cli join $ZEROTIER_NETWORK_ID
            """
            call_system(setup_command)

        def check(self):
            return True

    class Zsh:
        name = "zsh"
        depends_on = ["git"]
        links = {
            ".zsh": "~/.zsh",
            ".zshrc": "~/.zshrc",
            ".scripts": "~/.scripts",
        }

        def install(self):
            install_pacman("zsh")

        def setup(self):
            setup_symlink(self)
            submodule_path = f"{USER_PATH}/.zsh/antigen"
            if not os.path.exists(submodule_path):
                call_system(
                    f"git clone https://github.com/zsh-users/antigen.git {submodule_path}"
                )

            call_system("mkdir -p ~/.credentials; touch ~/.credentials/secure")
            return True

        def check(self):
            return True

    class Node:
        name = "node"
        depends_on = []
        links = {}

        def install(self):
            install_pacman("nodejs")
            install_pacman("npm")

        def setup(self):
            return True

        def check(self):
            return True

    class Telegram:
        name = "telegram"
        depends_on = ["flatpak"]
        links = {}

        def install(self):
            install_flatpak("telegram")

        def setup(self):
            return True

        def check(self):
            return True

    class Spotify:
        name = "spotify"
        depends_on = ["flatpak"]
        links = {}

        # NOTE Currently disabled
        def install(self):
            # install_flatpak("spotify")
            return True

        def setup(self):
            return True

        def check(self):
            return True

    class Fondo:
        name = "fondo"
        depends_on = ["flatpak"]
        links = {}

        def install(self):
            install_flatpak("fondo")

        def setup(self):
            return True

        def check(self):
            return True

    class Dbeaver:
        name = "dbeaver"
        depends_on = ["flatpak"]
        links = {}

        def install(self):
            install_flatpak("io.dbeaver.DBeaverCommunity")

        def setup(self):
            return True

        def check(self):
            return True

    class Pikaur:
        name = "pikaur"
        depends_on = ["arch_base", "python"]
        links = {}

        def install(self):
            if not self.check():
                install_command = """
                    cd /tmp
                    git clone https://aur.archlinux.org/pikaur.git
                    cd pikaur
                    makepkg -fsri --noconfirm
                """
                call_system(install_command)
            return True

        def setup(self):
            return True

        def check(self):
            response = call_system("type pikaur", exit_on_error=False)
            return response

    #     class Toptracker:
    #         name = "toptracker"
    #         depends_on = ["pikaur"]
    #         links = {}

    #         def install(self):
    #             install_aur("toptracker")

    #         def setup(self):
    #             return True

    #         def check(self):
    #             return True

    # class Insomnia:
    #     name = "insomnia"
    #     depends_on = ["pikaur"]
    #     links = {}

    #     def install(self):
    #         install_aur("insomnia")

    #     def setup(self):
    #         return True

    #     def check(self):
    #         return True

    class Openvpn:
        name = "openvpn"
        depends_on = []
        links = []

        def install(self):
            install_pacman("openvpn")

        def setup(self):
            return True

        def check(self):
            return True


if __name__ == "__main__":
    os.system("echo !!! Running as user `whoami`")
    installation = Installation()
    installation.install_all()
