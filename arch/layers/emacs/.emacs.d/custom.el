(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("a22f40b63f9bc0a69ebc8ba4fbc6b452a4e3f84b80590ba0a92b4ff599e53ad0" default)))
 '(git-gutter:hide-gutter t)
 '(git-gutter:lighter " gitgutter")
 '(git-gutter:modified-sign "~")
 '(git-gutter:update-interval 0.2)
 '(package-selected-packages
   (quote
    (zenburn csv-mode nord-theme company-lsp lsp-ui lsp-mode docker-compose-mode dockerfile-mode git-gutter company counsel ivy ripgrep projectile flycheck magit smartparens gruvbox-theme which-key use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(provide 'custom)
;;; custom.el ends here
