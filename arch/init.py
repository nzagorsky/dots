#!/usr/bin/env python3
import os
import subprocess
# import pickle

DATABASE_PATH = "/var/cache/dotfiles"
DATABASE_NAME = f"{DATABASE_PATH}/db.pickle"


def _detect_install():
    response = subprocess.check_output(["df"])
    parsed_response = response.decode("utf-8")
    return "archiso" in parsed_response and "cowspace" in parsed_response


def _layers_db_exists():
    def _db_makedir():
        response, err = subprocess.Popen(
            ["mkdir", "-p", DATABASE_PATH],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        ).communicate()
        return response, err

    response, err = _db_makedir()
    if err:
        if "permission denied" in err.decode("utf-8").lower():
            os.system(
                f"""
                      sudo mkdir {DATABASE_PATH};
                      sudo chown -R `whoami`:`whoami` {DATABASE_PATH}
                      """
            )
            response, err = _db_makedir()

    if err:
        raise SyntaxError(err.decode("utf-8"))

    return os.path.exists(DATABASE_NAME)


def main():
    installation_is_running = _detect_install()
    layers_db_exists = _layers_db_exists()

    if installation_is_running:
        # TODO go with aui/fifo
        pass
    else:
        # TODO consider aui/lifo (at least test it)
        pass

    print(f"Installation running: {installation_is_running}")
    print(f"Layers DB exists: {layers_db_exists}")


if __name__ == "__main__":
    main()
